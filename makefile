CC=gcc

all: regexTest

regexTest: regexTest.cpp
	mkdir -p build
	gcc -o build/$@ -I /usr/local/opt/boost\@1.59/include/ regexTest.cpp
clean:
	rm -rf build
	$(RM) a.out